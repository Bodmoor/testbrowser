﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BrowserTester
{
    [TestClass]
    public class Browse
    {
        private TestForm f;

        [TestMethod]
        public void LoadScriptAndPrint()
        {
            f = new TestForm();

            f.chroom1.Resize(0, 0);

            f.chroom1.EOView.CustomUserAgent = "";

            f.chroom1.DevHandle = f.devPanel.Handle;

            f.splitContainer1.SplitterMoved += (s, e) =>
            {
                f.chroom1.Resize(0, 0);
            };

            f.splitContainer1.SizeChanged += (s, e) =>
            {
                f.chroom1.Resize(0, 0);
            };

            //f.chroom1.GoTo(@"C:\Users\psegerink\Desktop\rad8ED97.html");
            f.chroom1.GoTo(@"https://www.linkedin.com/in/segerinkp");

            //f.btnPrint.Click += BtnPrint_Click;
            f.btnRefresh.Click += (s, e) =>
            {
                f.chroom1.GoTo(@"https://www.linkedin.com/in/segerinkp");
            };

            f.ShowDialog();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            f.chroom1.PrintPreview();
            //var test = f.chroom1.EvalScript("$(\"*\").printArea({mode:\"popup\", popTitle:\"This is PrintArea Demo\",popClose: false, removeScripts:true});");
        }
    }
}