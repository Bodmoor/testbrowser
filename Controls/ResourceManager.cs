﻿using System;
using System.IO;
using System.Reflection;

namespace Controls
{
    internal static class ResourceManager
    {
        internal static string GetResource(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            foreach (var rsn in assembly.GetManifestResourceNames())
            {
                if (rsn.EndsWith(resourceName))
                {
                    using (Stream stream = assembly.GetManifestResourceStream(rsn))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            return string.Empty;
        }

        internal static string SaveToTemp(string content, string fileName)
        {
            var path = Path.Combine(Path.GetTempPath(), fileName);

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            File.WriteAllText(path, content);

            return path;
        }
    }
}