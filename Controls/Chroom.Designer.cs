﻿namespace Controls
{
    partial class Chroom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            try
            {
                initWebView.Destroy();
                EOView.Destroy();
                EOControl.Dispose();
                Dispose();
            }
            catch 
            {
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EOControl = new EO.WinForm.WebControl();
            this.EOView = new EO.WebBrowser.WebView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // EOControl
            // 
            this.EOControl.BackColor = System.Drawing.Color.White;
            this.EOControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EOControl.Location = new System.Drawing.Point(0, 0);
            this.EOControl.Name = "EOControl";
            this.EOControl.Size = new System.Drawing.Size(87, 82);
            this.EOControl.TabIndex = 0;
            this.EOControl.Text = "EOControl";
            this.EOControl.WebView = this.EOView;
            // 
            // EOView
            // 
            this.EOView.ObjectForScripting = null;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.EOControl);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(87, 82);
            this.panel1.TabIndex = 1;
            // 
            // Chroom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "Chroom";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public EO.WebBrowser.WebView EOView;
        public EO.WinForm.WebControl EOControl;
        private System.Windows.Forms.Panel panel1;
    }
}
