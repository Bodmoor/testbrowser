﻿using System;
using System.Runtime.InteropServices;

namespace Controls
{
    [ComImport()]
    [Guid("76046E7A-7CEC-45BE-98CF-8E8AFBFFBEAA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    interface IObjectSafety
    {
        [PreserveSig()]
        int GetInterfaceSafetyOptions(ref Guid riid, out int pdwSupportedOptions, out int pdwEnabledOptions);

        [PreserveSig()]
        int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionSetMask, int dwEnabledOptions);
    }
}
