﻿using System;
using System.Runtime.InteropServices;

namespace Controls
{
    [ComVisible(true)]
    [Guid("D75EB606-163B-41B7-AA4A-25C631C1D11E")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IWebBrowserControlEvents
    {
        [DispId(0x10020003)]
        void OnUrlLoaded(string url, int httpStatusCode);

        [DispId(0x10020005)]
        void OnError(string message);

        [DispId(0x10020006)]
        void OnBeforeNavigate(object sender, string url, string frameName, ref bool Cancel);

        [DispId(0x10020007)]
        void OnProcessData(object sender, string actionName, string postData);

        [DispId(0x10020008)]
        void OnKeyDown(int keyCode, bool alt, bool ctrl, bool shift, ref bool handled);
    }

    [ComVisible(true)]
    [Guid("0F5DBE79-EF7C-4AC3-98D9-BDDF89F05DE2")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IWebBrowserControls
    {
        [DispId(0x10020011)]
        void GoTo(string url);

        [DispId(0x10020012)]
        void Resize(int width, int height);

        [DispId(0x10020013)]
        object EvalScript(string script);

        [DispId(0x10020014)]
        void Refresh();

        [DispId(0x10020015)]
        void PrintPage();

        [DispId(0x10020016)]
        void PrintPreview();

        [DispId(0x10020017)]
        string GetHtml();

        [DispId(0x10020018)]
        void PreLoad();

        [DispId(0x10020019)]
        void Destroy();
    }
}