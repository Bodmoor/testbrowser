﻿using EO.WebBrowser;
using Microsoft.Win32;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Controls
{
    [ComVisible(true)]
    [ComSourceInterfaces(typeof(IWebBrowserControlEvents))]
    [ProgId("Controls.Chroom")]
    [ClassInterface(ClassInterfaceType.None)]
    public partial class Chroom : UserControl, IWebBrowserControls, IObjectSafety
    {
        private WebView initWebView;
        #region delegates

        public delegate void OnBeforeNavigateDelegate(object sender, string url, string frameName, ref bool Cancel);

        private event OnBeforeNavigateDelegate OnBeforeNavigate;

        public delegate void OnUrlLoadedDelegate(string url, int httpStatusCode);

        private event OnUrlLoadedDelegate OnUrlLoaded;

        public delegate void OnKeyDownDelegate(int keyCode, bool alt, bool ctrl, bool shift, ref bool handled);

        private new event OnKeyDownDelegate OnKeyDown;

        public delegate void OnProcessDataDelegate(object sender, string actionName, string postData);

        private event OnProcessDataDelegate OnProcessData;

        public delegate void OnErrorDelegate(string message);

        private event OnErrorDelegate OnError;

        #endregion

        public bool CustomContent { get; internal set; }
        public string PostData { get; internal set; }
        public string Url { get; private set; }

        public IntPtr DevHandle { get; set; }

        public Chroom()
        {
            InitializeComponent();

            EOView.CustomUserAgent = "CRMCompany browser";
            EOView.RegisterJSExtensionFunction("PostData", new JSExtInvokeHandler(PostDataHandler));

            EOControl.KeyDown += EOControl_KeyDown;

            EO.WebEngine.BrowserOptions bo = new EO.WebEngine.BrowserOptions();

            EOView.NewWindow += EOView_NewWindow;

            EO.Base.Runtime.Exception += (s, e) =>
            {
            };

            EOView.LoadCompleted += (s, e) =>
            {
                OnUrlLoaded?.Invoke(e.Url, e.HttpStatusCode);
            };

            int shiftGeeId = CommandIds.RegisterUserCommand("ShiftGee");
            int devTools = CommandIds.RegisterUserCommand("devTools");

            EOView.Shortcuts = new EO.WebBrowser.Shortcut[]
            {
                new EO.WebBrowser.Shortcut(shiftGeeId, KeyCode.G, false, false, true),
                new EO.WebBrowser.Shortcut(devTools, KeyCode.F12, true, false, false)
            };

            EOView.Command += (s, e) =>
            {
                if (e.CommandId == shiftGeeId)
                {
                    var c = false;
                    OnKeyDown?.Invoke((int)Keys.G, false, false, true, ref c);
                }
                else if (e.CommandId == devTools)
                {
                    /* opens the developer tools in a separate window */
                    var dt = new DevTools();

                    dt.Show();

                    EOView.ShowDevTools(dt.pTools.Handle);
                }
            };


            EOView.BeforeNavigate += (s, e) =>
            {
                if (e.NewUrl.StartsWith("uigrid") || e.NewUrl.StartsWith("about"))
                {
                    e.Cancel = true;
                }
                else
                {
                    bool c = e.Cancel;
                    OnBeforeNavigate?.Invoke(s, e.NewUrl, e.FrameName, ref c);
                    e.Cancel = c;
                }
            };
        }

        ~Chroom()
        {
            try
            {
                initWebView.Destroy();
                EOView.Destroy();
                EOControl.Dispose();
                Dispose();
            }
            catch (Exception)
            {
            }
        }

        private void EOView_NewWindow(object sender, NewWindowEventArgs e)
        {
            /* open this in a new popup in a new control and set e.Accepted to true. */
        }

        /// <summary>
        /// send to in2crm to handle keypresses as normal
        /// </summary>
        /// <param name="sender">the control</param>
        /// <param name="e">event arguments</param>
        private void EOControl_KeyDown(object sender, KeyEventArgs e)
        {
            bool c = e.Handled;
            OnKeyDown?.Invoke(e.KeyValue, e.Alt, e.Control, e.Shift, ref c);
            e.Handled = c;
        }

        /* this function in called from the javascript loaded into the webpage, as an alternative to navigation events
         * can handle alot more data as parameters */
        private void PostDataHandler(object sender, JSExtInvokeArgs e)
        {
            OnProcessData?.Invoke(EOControl, e.Arguments[0].ToString(), e.Arguments[1].ToString());
        }

        /// <summary>
        /// loads the supplied url into a webview, or saves raw html to a temp file.
        /// </summary>
        /// <param name="url">the url or html to display</param>
        public void GoTo(string url)
        {
            Uri uriResult;
            if (Uri.TryCreate(url, UriKind.Absolute, out uriResult))
            {
                /* this is a valid url */
                Url = url;
            }
            else
            {
                /* assume raw html
                 * load from a file to ensure images and other resources that refer to a physical location also work properly */
                Url = ResourceManager.SaveToTemp(url, Path.GetTempFileName());
                Url = Url;
            }

            EOView.LoadUrl(Url);
        }

        /// <summary>
        /// used to set the containing panel to the proper size. This is needed to compensate for applications where another format is used for size, 
        /// instead of pixels. TWIPS is an example
        /// </summary>
        /// <param name="width">width in pixels</param>
        /// <param name="height">height in pixels</param>
        public new void Resize(int width, int height)
        {
            if (width == 0 && height == 0)
            {
                panel1.Width = Width;
                panel1.Height = Height;
            }
            else
            {
                panel1.Width = width;
                panel1.Height = height;
            }
        }

        /// <summary>
        /// executes the supplied script on the loaded DOM
        /// </summary>
        /// <param name="script">the script to execute</param>
        /// <returns>the return value of the eval code</returns>
        public object EvalScript(string script)
        {
            return EOView.EvalScript(script);
        }

        /// <summary>
        /// reloads the content of the browser
        /// </summary>
        public new void Refresh()
        {
            if (!string.IsNullOrEmpty(Url))
            {
                EOView.LoadUrlAndWait(Url);
            }
        }

        /// <summary>
        /// loads the html into an (old) IE browser, which then calls its print preview dialog.
        /// </summary>
        public void PrintPreview()
        {
            var wb = new WebBrowser();

            wb.Dock = DockStyle.Fill;

            wb.ScriptErrorsSuppressed = true;

            Controls.Add(wb);

            wb.DocumentText = EOView.GetHtml();

            wb.DocumentCompleted += (s, e) =>
            {
                wb.ShowPrintPreviewDialog();

                wb.Dispose();
            };
        }


        /// <summary>
        /// prints the HTML
        /// </summary>
        public void PrintPage()
        {
            EOView.Print();
        }

        /// <summary>
        /// returns the current html
        /// </summary>
        /// <returns>the html as a string</returns>
        public string GetHtml()
        {
            return EOView.GetHtml();
        }

        public void PreLoad()
        {
            ThreadRunner threadRunner = new ThreadRunner();
            initWebView = threadRunner.CreateWebView();
        }

        public void Destroy()
        {
            try
            {
                initWebView.Destroy();
                EOView.Destroy();
                EOControl.Dispose();
                Dispose();
            }
            catch
            {
            }
        }

        #region COM interface necessities


        [ComRegisterFunction()]
        public static void RegisterClass(string key)
        {
            StringBuilder skey = new StringBuilder(key);
            skey.Replace(@"HKEY_CLASSES_ROOT\", "");

            Type myType1 = Type.GetTypeFromProgID("Controls.Chroom");
            Console.WriteLine("ProgID=Controls.Chroom GUID={0}.", myType1.GUID);

            TextWriter tw;
            tw = File.CreateText("guid.txt");
            tw.WriteLine(skey.ToString());
            tw.WriteLine(myType1.GUID.ToString());
            tw.Close();

            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(skey.ToString(), true);
            RegistryKey ctrl = regKey.CreateSubKey("Control");
            ctrl.Close();
            RegistryKey inprocServer32 = regKey.OpenSubKey("InprocServer32", true);
            inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
            inprocServer32.Close();
            regKey.Close();
        }

        // Unregister COM ActiveX object
        [ComUnregisterFunction()]
        public static void UnregisterClass(string key)
        {
            StringBuilder skey = new StringBuilder(key);
            skey.Replace(@"HKEY_CLASSES_ROOT\", "");
            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(skey.ToString(), true);
            regKey.DeleteSubKey("Control", false);
            RegistryKey inprocServer32 = regKey.OpenSubKey("InprocServer32", true);
            regKey.DeleteSubKey("CodeBase", false);
            regKey.Close();
        }

        public enum ObjectSafetyOptions
        {
            INTERFACESAFE_FOR_UNTRUSTED_CALLER = 0x00000001,
            INTERFACESAFE_FOR_UNTRUSTED_DATA = 0x00000002,
            INTERFACE_USES_DISPEX = 0x00000004,
            INTERFACE_USES_SECURITY_MANAGER = 0x00000008
        };

        public int GetInterfaceSafetyOptions(ref Guid riid, out int pdwSupportedOptions, out int pdwEnabledOptions)
        {
            ObjectSafetyOptions m_options = ObjectSafetyOptions.INTERFACESAFE_FOR_UNTRUSTED_CALLER | ObjectSafetyOptions.INTERFACESAFE_FOR_UNTRUSTED_DATA;
            pdwSupportedOptions = (int)m_options;
            pdwEnabledOptions = (int)m_options;
            return 0;
        }

        public int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionSetMask, int dwEnabledOptions)
        {
            return 0;
        }

        #endregion
    }
}