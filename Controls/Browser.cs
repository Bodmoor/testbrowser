﻿using DotNetBrowser;
using DotNetBrowser.WinForms;
using Microsoft.Win32;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Controls
{
    [ComVisible(true)]
    [ComSourceInterfaces(typeof(IWebBrowserControlEvents))]
    [ProgId("Controls.NETBrowser")]
    [ClassInterface(ClassInterfaceType.None)]
    public partial class NETBrowser: UserControl, IWebBrowserControls, IObjectSafety
    {
        private Browser browser;

        public NETBrowser()
        {
            try
            {
                InitializeComponent();

                BrowserContext browserContext = BrowserContext.DefaultContext;
                browserContext.NetworkService.NetworkDelegate = new NetworkEvents();

                browser = BrowserFactory.Create(BrowserType.LIGHTWEIGHT);
                BrowserView browserView = new WinFormsBrowserView(browser);

                browser.LoadHandler = new CustomLoadHandler();

                var ctrl = (Control)browserView;
                ctrl.Dock = DockStyle.Fill;
                panel1.Controls.Add(ctrl);

                browser.FinishLoadingFrameEvent += Browser_FinishLoadingFrameEvent;

                browser.Context.NetworkService.ResourceHandler = new CustomResourceHandler();
            }
            catch (Exception ex)
            {
                OnError?.Invoke(ex.Message);
            }
        }

        private void Browser_FinishLoadingFrameEvent(object sender, DotNetBrowser.Events.FinishLoadingEventArgs e)
        {
            try
            {
                if (e.IsMainFrame)
                {
                    JSValue value = browser.ExecuteJavaScriptAndReturnValue("window");
                    value.AsObject().SetProperty("PostData", new CustomPostData(this));
                }

                OnUrlLoaded?.Invoke();
            }
            catch (Exception ex)
            {
                OnError?.Invoke(ex.Message);
            }
        }

        ~NETBrowser()
        {
            browser.Dispose();
        }

        public Browser wb
        {
            get
            {
                return browser;
            }
        }

        public int PanelHeight
        {
            get
            {
                return panel1.Height;
            }

            set
            {
                panel1.Height = value;
            }
        }

        public int PanelWidth
        {
            get
            {
                return panel1.Width;
            }

            set
            {
                panel1.Width = value;
            }
        }

        public enum ObjectSafetyOptions
        {
            INTERFACESAFE_FOR_UNTRUSTED_CALLER = 0x00000001,
            INTERFACESAFE_FOR_UNTRUSTED_DATA = 0x00000002,
            INTERFACE_USES_DISPEX = 0x00000004,
            INTERFACE_USES_SECURITY_MANAGER = 0x00000008
        };

        public int GetInterfaceSafetyOptions(ref Guid riid, out int pdwSupportedOptions, out int pdwEnabledOptions)
        {
            ObjectSafetyOptions m_options = ObjectSafetyOptions.INTERFACESAFE_FOR_UNTRUSTED_CALLER | ObjectSafetyOptions.INTERFACESAFE_FOR_UNTRUSTED_DATA;
            pdwSupportedOptions = (int)m_options;
            pdwEnabledOptions = (int)m_options;
            return 0;
        }

        public int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionSetMask, int dwEnabledOptions)
        {
            return 0;
        }

        public void GoTo(string url)
        {
            try
            {
                browser.LoadURL(url);
            }
            catch (Exception ex)
            {
                OnError?.Invoke(ex.Message);
            }
        }

        [ComRegisterFunction()]
        public static void RegisterClass(string key)
        {
            StringBuilder skey = new StringBuilder(key);
            skey.Replace(@"HKEY_CLASSES_ROOT\", "");

            Type myType1 = Type.GetTypeFromProgID("Controls.NETBrowser");
            Console.WriteLine("ProgID=Controls.NETBrowser GUID={0}.", myType1.GUID);

            TextWriter tw;
            tw = File.CreateText("guid.txt");
            tw.WriteLine(skey.ToString());
            tw.WriteLine(myType1.GUID.ToString());
            tw.Close();

            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(skey.ToString(), true);
            RegistryKey ctrl = regKey.CreateSubKey("Control");
            ctrl.Close();
            RegistryKey inprocServer32 = regKey.OpenSubKey("InprocServer32", true);
            inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
            inprocServer32.Close();
            regKey.Close();
        }


        // Unregister COM ActiveX object
        [ComUnregisterFunction()]
        public static void UnregisterClass(string key)
        {
            StringBuilder skey = new StringBuilder(key);
            skey.Replace(@"HKEY_CLASSES_ROOT\", "");
            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(skey.ToString(), true);
            regKey.DeleteSubKey("Control", false);
            RegistryKey inprocServer32 = regKey.OpenSubKey("InprocServer32", true);
            regKey.DeleteSubKey("CodeBase", false);
            regKey.Close();
        }

        public delegate void OnUrlLoadedDelegate();

        private event OnUrlLoadedDelegate OnUrlLoaded;

        public delegate void OnErrorDelegate(string message);

        private event OnErrorDelegate OnError;

        public delegate void PostDataDelegate(string action, string content);

        private event PostDataDelegate OnPostData;

        internal void InvokePostData(string action, string content)
        {
            OnPostData?.Invoke(action, content);
        }

        public void DisposeBrowser()
        {
            browser.Dispose();
            Dispose();

            GC.Collect();
        }

        void IWebBrowserControls.Resize(int width, int height)
        {
            panel1.Width = width;
            panel1.Height = height;
        }
    }

    public class CustomPostData
    {
        private NETBrowser nETBrowser;

        public CustomPostData(NETBrowser nETBrowser)
        {
            this.nETBrowser = nETBrowser;
        }

        public void Save(string action, string content)
        {

            nETBrowser.InvokePostData(action, content);
        }
    }
}